

------

环境：

[jkd8+]()

[mysql5.6+]()

[flowable6.4.0]()

## 一、原理

- 触发信号事件

  既可以通过bpmn节点由流程实例触发一个信号，也可以通过API触发。

  手工触发一个信号:

```java
//把信号发送给全局所有订阅的处理器（广播语义）。
runtimeService.signalEventReceived(String signalName);
//只把信息发送给指定的执行。
runtimeService.signalEventReceived(String signalName, String executionId);
```



- 捕获信号事件

  信号事件可以被**中间捕获信号事件**或**边界信息事件**捕获。

- 查询信号事件的订阅

  可以查询所有订阅了特定信号事件的执行：

```java
 List<Execution> executions = runtimeService.createExecutionQuery()
      .signalEventSubscriptionName("alert")
      .list();
                
```

​	可以使用signalEventReceived(String signalName, String executionId)方法 吧信号发送给这些执行实例执行。

- 信号事件范围

  默认，信号会在*流程引擎范围内进行广播*。就是说， 你可以在一个流程实例中抛出一个信号事件，其他不同流程定义的流程实例 都可以监听到这个事件。

  然而，有时只希望在*同一个流程实例*中响应这个信号事件。 比如一个场景是，流程实例中的同步机制，如果两个或更多活动是互斥的。

  如果想要限制信号事件的*范围*，可以使用信号事件定义的*scope 属性* （不是BPMN2.0的标准属性）：

```x&#39;m
<signal id="alertSignal" name="alert" activiti:scope"processInstance"/>
```

## 二、流程图

- 抛出全局信号事件

![](./images/throwglobalevent.png)

- 捕获信号事件

![](./images/catchglobalevent.png)

- 抛出流程范围内信号事件

![](./images/throwandcatchprocessevent.png)

- 捕获边界事件定义

  **cancelActivity**指定捕获边界事件定义**所在的节点**是否取消活动任务，如果配置为true时，当捕获到边界事件后，此节点让任务就会删除。

```
<boundaryEvent id="catprocesssingalevent" name="捕获流程内部的信号" cancelActivity="false" attachedToRef="otherbusinessdeal">
      <signalEventDefinition signalRef="alertSignal"></signalEventDefinition>
</boundaryEvent>
```



## 三、配置

- 信号事件定义

  flowable:scope指定事件范围,取值有global/processInstance，默认全局即所有流程都可以捕获到事件。

  ```
  <signal id="alertSignal" name="alert" flowable:scope="processInstance"/>
  ```

- 抛出信号事件定义

  **signalRef属性指定信号事件定义id**

```
<intermediateThrowEvent id="Throwsignal" name="抛出信号事件">
      <signalEventDefinition signalRef="alertSignal"></signalEventDefinition>
</intermediateThrowEvent>
```





## 四、实践测试



- 分别部署抛出信号事件、捕获全局事件、流程范围信号事件。

  部署抛出信号事件:

  ```
  deployThrowglobalsignalevent()
  ```

  部署捕获全局事件:

  ```
  deployCatchglobalevent()
  ```

  部署流程范围信号事件:

  ```
  deployProcesssignalevent()
  ```

- 启动抛出信号事件流程实例、捕获全局事件流程、流程范围信号事件实例。

  启动抛出信号事件流程实例:

  ```
  startThrowglobalsignalevent()
  ```

  启动捕获全局事件流程实例:

  ```
  startCatchglobalevent()
  ```

  启动流程范围信号事件实例:

  ```
  startProcesssignalevent()
  ```

- 完成图1中【修改规则】用户节点任务。

  ​	**可以看到当完成此用户任务后，由于图1定义的信号事件范围是全局的，会抛出全局信号事件,所有流程实例中的边界捕获信号事件都会捕获到此信号；**

  ​	**因此图2、图3中的边界捕获信号事件会捕获到信号，并交给各自服务任务处理，两个服务任务均输出了日志信息。**

- 完成图2中【修改规则】用户节点任务。

  ​	**可以看到当完成此用户任务后，由于图2定义的信号事件范围是流程实例级别的，会抛出信号事件到当前流程实例。**

  ​	**因此只有图3中的边界捕获信号事件会捕获到信号，并交给对应的服务任务处理，所以只有图3中的服务任务输出了日志信息。**

